from django import forms
from .models import *

class PersonForm(forms.ModelForm):
    class Meta:
        model=Person
        fields=fields = '__all__'

class CourseForm(forms.ModelForm):
    class Meta:
        model=Course
        fields=fields = '__all__'

class TrainingForm(forms.ModelForm):
    class Meta:
        model=Training
        fields=fields = '__all__'

class TitleForm(forms.ModelForm):
    class Meta:
        model=Title
        fields=fields = '__all__'

class ExperienceForm(forms.ModelForm):
    class Meta:
        model=Experience
        fields=fields = '__all__'

class SkillForm(forms.ModelForm):
    class Meta:
        model=Skill
        fields=fields = '__all__'