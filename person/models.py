# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
import datetime

GENDER = (('F', 'FEMENINO'), ('M', 'MASCULINO'))

class Person(models.Model):
    ci = models.CharField(max_length=12, unique=True, verbose_name='CI')
    name = models.CharField(max_length=100, default='', verbose_name='Nombres')
    middleName = models.CharField(max_length=100, blank=True, null=True, verbose_name='Apellido Paterno')
    lastName = models.CharField(max_length=100, blank=True, null=True, verbose_name='Apellido Materno')
    gender = models.CharField(max_length=2, choices=GENDER, verbose_name=u'Género')
    homeAddress = models.CharField(max_length=100, blank=True, null=True, verbose_name=u'Dirección')
    city = models.CharField(max_length=100, blank=True, null=True, verbose_name='Ciudad')
    dateBirth = models.DateField(auto_now_add=False, verbose_name='Fecha de Nacimiento')
    mobilePhone = models.CharField(max_length=25, blank=True, null=True, verbose_name='Nro. de Celular')
    email = models.CharField(max_length=25, blank=True, null=True, verbose_name='Correo')
    registerAt = models.DateField(auto_now_add=True, verbose_name='Fecha de registro')

    def __unicode__(self):
        return "%s" % self.name

    def __str__(self):
        return "%s" % self.name

    class Meta:
        verbose_name = 'Persona'
        verbose_name_plural = 'Personas'

        permissions = (
            ('show_person', 'Can Details Person'),
            ('index_person', 'Can List Person'),
        ),


class Course(models.Model):
    dateInit = models.DateField(auto_now_add=False, verbose_name='Fecha de inicio')
    dateFinish = models.DateField(auto_now_add=False, verbose_name=u'Fecha de finalización')
    courseName = models.CharField(max_length=100, default='', verbose_name='Nombre del curso')
    description = models.CharField(max_length=100, default='', verbose_name=u'Descripción')
    person = models.ForeignKey(Person, blank=True, null=True, verbose_name='Persona', on_delete=models.CASCADE)

    def __unicode__(self):
        return "%s" % self.courseName

    def __str__(self):
        return "%s" % self.courseName

    class Meta:
        verbose_name = 'Curso'
        verbose_name_plural = 'Cursos'

        permissions = (
                          ('show_course', 'Can Details Course'),
                          ('index_course', 'Can List Course'),
                      ),


class Training(models.Model):
    dateInit = models.DateField(auto_now_add=False, verbose_name='Fecha de inicio')
    dateFinish = models.DateField(auto_now_add=False, verbose_name=u'Fecha de finalización')
    level= models.CharField(max_length=100, default='', verbose_name='Nivel')
    institution = models.CharField(max_length=100, default='', verbose_name=u'Institución')
    description = models.CharField(max_length=100, default='', verbose_name=u'Descripción')
    person = models.ForeignKey(Person, blank=True, null=True, verbose_name='Persona', on_delete=models.CASCADE)

    def __unicode__(self):
        return "%s" % self.institution

    def __str__(self):
        return "%s" % self.institution

    class Meta:
        verbose_name = 'Formacion'
        verbose_name_plural = 'Formaciones'

        permissions = (
                          ('show_training', 'Can Details Training'),
                          ('index_training', 'Can List Training'),
                      ),

class Title(models.Model):
    dateObtained = models.DateField(auto_now_add=False, verbose_name=u'Fecha de obteción')
    nameTitle = models.CharField(max_length=100, default='', verbose_name=u'Título')
    description = models.CharField(max_length=100, default='', verbose_name=u'Descripción')
    person = models.ForeignKey(Person, blank=True, null=True, verbose_name='Persona', on_delete=models.CASCADE)

    def __unicode__(self):
        return "%s" % self.nameTitle

    def __str__(self):
        return "%s" % self.nameTitle

    class Meta:
        verbose_name = 'Titulo'
        verbose_name_plural = 'Titulos'

        permissions = (
                          ('show_title', 'Can Details Title'),
                          ('index_title', 'Can List Title'),
                      ),

class Experience(models.Model):
    dateInit = models.DateField(auto_now_add=False, verbose_name='Fecha de inicio')
    dateFinish = models.DateField(auto_now_add=False, verbose_name=u'Fecha de finalización')
    institution = models.CharField(max_length=100, default='', verbose_name=u'Intitución')
    charge = models.CharField(max_length=100, default='', verbose_name='Cargo')
    description = models.CharField(max_length=100, default='', verbose_name=u'Descripción')
    person = models.ForeignKey(Person, blank=True, null=True, verbose_name='Persona', on_delete=models.CASCADE)

    def __unicode__(self):
        return "%s" % self.institution

    def __str__(self):
        return "%s" % self.institution

    class Meta:
        verbose_name = 'Exeriencia'
        verbose_name_plural = 'Experiencias'

        permissions = (
                          ('show_experience', 'Can Details Experience'),
                          ('index_experience', 'Can List Experience'),
                      ),

class Skill(models.Model):
    nameSkill = models.CharField(max_length=100, default='', verbose_name='Habilidad')
    level = models.IntegerField(default=0, verbose_name='Nivel')
    description = models.CharField(max_length=100, default='', verbose_name=u'Descripción')
    person = models.ForeignKey(Person, blank=True, null=True, verbose_name='Persona', on_delete=models.CASCADE)

    def __unicode__(self):
        return "%s" % self.nameSkill

    def __str__(self):
        return "%s" % self.nameSkill

    class Meta:
        verbose_name = 'Habilidad'
        verbose_name_plural = 'Habilidades'

        permissions = (
                          ('show_skill', 'Can Details Skill'),
                          ('index_skill', 'Can List Skill'),
                      ),
