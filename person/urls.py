from django.conf.urls import url
from django.urls import path, include
from person import views
urlpatterns=[
    path('courses/', include([
        path('',views.indexCourse, name='courses-index'),
        path('new/',views.newCourse, name='courses-new'),

    ])),
]