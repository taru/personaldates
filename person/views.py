from django.shortcuts import render
from .models import *
from .forms import CourseForm

from django.contrib import messages
from django.urls import reverse
from django.http import HttpResponseRedirect

def home(request):
    person = Person.objects.get(id=1)
    courses = Course.objects.filter(person_id=person.id)
    training= Training.objects.filter(person_id=person.id)
    titles= Title.objects.filter(person_id=person.id)
    experiences= Experience.objects.filter(person_id=person.id)
    skills= Skill.objects.filter(person_id=person.id)

    return render(request, 'home.html', {
        'experiences':experiences,
        'titles':titles,
        'skills':skills,
        'training':training,
        'person': person,
        'courses': courses
    })

def indexCourse(request):
    courses = Course.objects.all
    return render(request, 'courses/index.html', {
        'courses': courses,
        'course_obj': Course
    })

def newCourse(request):
    if request.method=='POST':
        form=CourseForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse(indexCourse))
    else:
        form=CourseForm()
    return render(request,'courses/new.html',{'form':form})

